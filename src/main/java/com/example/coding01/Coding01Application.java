package com.example.coding01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Coding01Application {

	public static void main(String[] args) {
		SpringApplication.run(Coding01Application.class, args);
	}
}